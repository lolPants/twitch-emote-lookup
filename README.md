# Twitch Emotes Lookup ![](https://gitlab.com/lolPants/twitch-emote-lookup/badges/master/build.svg)
_API to lookup Twitch Emote IDs by name._  
Built by [Jack Baron](https://www.jackbaron.com) using [TwitchEmotes](https://twitchemotes.com)

## Usage
### Prerequisites
- [Docker](https://docs.docker.com/engine/installation/)
- [Docker Compose](https://docs.docker.com/compose/install/)

### Docker Registry
More info is available [here](https://gitlab.com/lolPants/twitch-emote-lookup/container_registry).

### Development
Start the service using `docker-compose up`  
This will start a Redis server and the Web instance. It starts running in the foreground. `CTRL+C` to stop. You can also run it in detatched mode with `docker-compose up -d`.  
When you're done, `docker-compose rm` to remove the leftover containers.

### Production
Start the service in detached mode with autoreload with `docker-compose -f docker-compose.yml -f production.yml up -d`
