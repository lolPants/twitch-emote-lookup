// Sync
const { syncEmotes } = require('./sync')

// Scheduler
const schedule = require('node-schedule')

// Express Setup
const express = require('express')
const app = express()

// Database
const Redis = require('ioredis')
const redis = new Redis(process.env.REDIS || 'redis')

// Logging
const morgan = require('morgan')
app.use(morgan('combined'))

// CORS
const cors = require('cors')
app.use(cors())

// Caching
const { cacheSeconds } = require('route-cache')
const CACHE_TIME = 60 * 10

// Documentation
app.get('/', (req, res) => {
  res.sendFile(require('path').join(__dirname, './index.txt'))
})

// [DEPRECATED] Lookup Endpoint
app.get('/api/v1.0/:name', cacheSeconds(CACHE_TIME), (req, res) => {
  res.redirect(301, `/api/v1.1/id/${req.params.name}`)
})

// Lookup Emote ID from Name
app.get('/api/v1.1/id/:name', cacheSeconds(CACHE_TIME), async (req, res) => {
  let id = await redis.get(`name:${req.params.name}`)
  if (id) {
    res.set('Content-type', 'text/plain')
    res.send(id)
  } else {
    res.sendStatus(404)
  }
})

// Lookup Emote Name from ID
app.get('/api/v1.1/name/:id', cacheSeconds(CACHE_TIME), async (req, res) => {
  let name = await redis.get(`id:${req.params.id}`)
  if (name) {
    res.set('Content-type', 'text/plain')
    res.send(name)
  } else {
    res.sendStatus(404)
  }
})

// Lookup Emote URL from Name
app.get('/api/v1.1/url/:name', cacheSeconds(CACHE_TIME), async (req, res) => {
  let id = await redis.get(`name:${req.params.name}`)
  if (id) {
    res.set('Content-type', 'text/plain')
    res.send(`https://static-cdn.jtvnw.net/emoticons/v1/${id}/3.0`)
  } else {
    res.sendStatus(404)
  }
})

// Lookup Emote Image from Name
app.get('/api/v1.1/image/:name', cacheSeconds(CACHE_TIME), async (req, res) => {
  let id = await redis.get(`name:${req.params.name}`)
  if (id) {
    let url = `https://static-cdn.jtvnw.net/emoticons/v1/${id}/3.0`
    res.redirect(301, url)
  } else {
    res.sendStatus(404)
  }
})

// Sync Every 30 Minutes and on launch
schedule.scheduleJob('*/30 * * * *', () => syncEmotes)
syncEmotes()

app.listen(process.env.PORT || 3000)
