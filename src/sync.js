// Database
const Redis = require('ioredis')
const redis = new Redis(process.env.REDIS || 'redis')

// Data Streaming
const es = require('event-stream')
const snekfetch = require('snekfetch')

/**
 * Sync *all* Twitch Emotes
 * @returns {Promise.<void>}
 */
const syncEmotes = () => new Promise(resolve => {
  let stream = snekfetch.get('https://twitchemotes.com/api_cache/v3/images.json')
    .pipe(es.split('},'))
    .pipe(es.map((data, cb) => {
      /**
       * @type {string}
       */
      let line = data
      line = line.replace(/"[0-9]+":/, '')
      line = line.replace('{{', '{')
      line += '}'
      line = line.replace('}}}', '}')

      let { id, code } = JSON.parse(line)
      redis.set(`name:${code}`, id)
      redis.set(`id:${id}`, code)

      return cb(null, null)
    }))

  stream.on('end', () => {
    resolve()
  })
})

// Export all functions
module.exports = { syncEmotes }
